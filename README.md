Unity에 유용한 클래스와 함수를 모아놓은 프로젝트입니다.

## FEATURES 
- Math
- Coroutine
- Unity Extension Method

## COMPATIBILITY
- Requires Unity 2018.3 or above
- Requires .NET 4.x

### CREATE BY
- Sanghun Lee (Unity Client Developer)
- Email: tkdgns1284@gmail.com
- Gitlab: https://gitlab.com/tkdgns1284