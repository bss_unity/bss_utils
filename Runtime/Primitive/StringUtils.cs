﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BSS.Utils {
    public static class StringUtils {
        /// <summary>
        /// Eg. (["Item1","Item2","Item3"] , ",") => "Item1,Item2,Item3"
        /// </summary>
        public static string ToSeparatedString(this IEnumerable enumerable, string separator) {
            return string.Join(separator, enumerable.Cast<object>().Select(o => o?.ToString() ?? "(null)").ToArray());
        }

        public static int GetLineCount(this string str) {
            if(str == null)
                throw new ArgumentNullException("str");
            if(str == string.Empty)
                return 0;
            int index = -1;
            int count = 0;
            while(-1 != (index = str.IndexOf(Environment.NewLine, index + 1)))
                count++;

            return count + 1;
        }
    }
}