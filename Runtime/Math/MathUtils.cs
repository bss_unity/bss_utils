using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BSS.Utils {
    public static class MathUtils 
    {
        public static int WeightedRandom(IEnumerable<float> list) {
            float sum = list.Sum();
            float r = Random.Range(0, sum);
            float checkVal = 0;
            int i = 0;
            foreach(var it in list) {
                checkVal += it;
                if(r < checkVal) {
                    return i;
                }
                i++;
            }
            return i - 1;
        }

        public static Vector3 Bezier3(Vector3 start, Vector3 mid, Vector3 end, float t) {
            Vector3 p1 = Vector3.Lerp(start, mid, t);
            Vector3 p2 = Vector3.Lerp(mid, end, t);
            Vector3 p3 = Vector3.Lerp(p1, p2, t);

            return p3;
        }
    }
}
